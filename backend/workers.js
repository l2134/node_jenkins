const express = require('express')
const db = require('./db')
const utils = require('./utils')
const router = express.Router()

// for adding new worker

router.post('/', (request, response) => {
    const { name,salary,age } = request.body
    const query = `
      INSERT INTO worker
        (name, salary,age)
      VALUES
        ('${name}','${salary}','${age}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

    //RMM for getting all worker

    router.get('/', (request, response) => {
        const query = `
          SELECT *
          FROM worker
        `
        db.execute(query, (error, result) => {
          response.send(utils.createResult(error, result))
        })
      })

      //RMM for update worker

      router.put('/:id', (request, response) => {
        const { id } = request.params
        const { name,salary,age } = request.body
      
        const query = `
          UPDATE worker
          SET
            name = '${name}', 
            salary = '${salary}',
            age = '${age}'
          WHERE
            id = ${id}
        `
        db.execute(query, (error, result) => {
          response.send(utils.createResult(error, result))
        })
      })
      
        //RMM for delete worker
  router.delete('/:id', (request, response) => {
    const { id } = request.params
  
    const query = `
      DELETE FROM worker
      WHERE
        id = ${id}
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

  module.exports = router
